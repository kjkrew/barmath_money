--[[
	BarMath 2.x Addon: Money Bar
	Options Functions
	Version <%version%>
	
	Revision: $Id: options.lua 7 2012-12-23 11:53:50 PST Kjasi $
]]

-- Assign Localized Text
function BarMath_Options_Money_OnLoad()
	-- Goals Page
	BarMath_MoneyOptions_SetGlobalTargetBoxTitle:SetText(BARMATH_MONEY_GLOBALTARGET);
	BarMath_MoneyOptions_SetPrivateTargetBoxTitle:SetText(BARMATH_MONEY_PRIVATETARGET);
	BarMath_MoneyOptions_SelfishModeText:SetText(BARMATH_MONEY_SELFISHMODE);
	BarMath_MoneyOptions_ExternalFundingTitle:SetText(BARMATH_MONEY_EXTERNALFUNDING);

	-- Smart Goals Page

	-- Money List Page

	-- Tooltips
	BarMath_MoneyOptions_SetGlobalTargetBoxTooltipTitle = BARMATH_MONEY_TOOLTIPS_SETGLOBALTITLE;
	BarMath_MoneyOptions_SetGlobalTargetBoxTooltipText = BARMATH_MONEY_TOOLTIPS_SETGLOBALTEXT;
	BarMath_MoneyOptions_SetPrivateTargetBoxTooltipTitle = BARMATH_MONEY_TOOLTIPS_SETPRIVATETITLE;
	BarMath_MoneyOptions_SetPrivateTargetBoxTooltipText = BARMATH_MONEY_TOOLTIPS_SETPRIVATETEXT;
	if (BarMath_GetCharVar("Bars","MoneyBar","SelfishMode") == 0) then
		BarMath_MoneyOptions_SelfishModeTooltipTitle = BARMATH_MONEY_TOOLTIPS_SELFISHMODEENABLETITLE;
	else
		BarMath_MoneyOptions_SelfishModeTooltipTitle = BARMATH_MONEY_TOOLTIPS_SELFISHMODEDISABLETITLE;
	end
	BarMath_MoneyOptions_SelfishModeTooltipText = BARMATH_MONEY_TOOLTIPS_SELFISHMODETEXT;
	BarMath_MoneyOptions_ExternalFundingTooltipTitle = BARMATH_MONEY_EXTERNALFUNDING;
	BarMath_MoneyOptions_ExternalFundingTooltipText = BARMATH_MONEY_TOOLTIPS_EXTERNALFUNDINGTEXT;

	-- Checkboxes
	BarMath_MoneyOptions_SelfishModeCheckBox:SetChecked(BarMath_GetCharVar("Bars","MoneyBar","SelfishMode"));

	-- Buttons
	BarMath_MoneyOptions_AddPrivateToGlobalBtnText:SetText(BARMATH_MONEY_ADDPRIVATETOGLOBAL);
	if (BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget") == 0) then
		BarMath_MoneyOptions_AddPrivateToGlobalBtn:Disable();
	else
		BarMath_MoneyOptions_AddPrivateToGlobalBtn:Enable();
	end
end

function BarMath_Money_SetGlobalTarget_OnShow()
	local target;

	if (BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"] == nil) then
		target = 0;
	else
		target = BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"];
	end

	BarMath_MoneyOptions_SetGlobalTargetBox:SetText(gsub(BarMath_CSG(target),",",""))
	BarMath_Money_Update();
end

function BarMath_Money_SetGlobalTarget_OnChange()
	local money;
	local input = BarMath_MoneyOptions_SetGlobalTargetBox:GetText();
	local input2 = BarMath_TranslateToMoney(input);

	if (input2 ~= nil) then
		money = input2;
	else
		money = tonumber(input);
	end

	if (money ~= nil) then
		BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"] = money;
	else
		BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"] = 0;
	end
	BarMath_MoneyOptions_SetGlobalTargetBox:SetText(gsub(BarMath_CSG(BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"]),",",""));
	BarMath_Money_Update();
end

function BarMath_Money_ExternalFunding_OnShow()
	local target;

	if (BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["AdditionalMoney"] == nil) then
		target = 0;
	else
		target = BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["AdditionalMoney"];
	end

	BarMath_MoneyOptions_ExternalFunding:SetText(gsub(BarMath_CSG(target),",",""))
	BarMath_Money_Update();
end

function BarMath_Money_ExternalFunding_OnChange()
	local money;
	local input = BarMath_MoneyOptions_ExternalFunding:GetText();
	local input2 = BarMath_TranslateToMoney(input);

	if (input2 ~= nil) then
		money = input2;
	else
		money = tonumber(input);
	end

	if (money ~= nil) then
		BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["AdditionalMoney"] = money;
	else
		BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["AdditionalMoney"] = 0;
	end
	BarMath_MoneyOptions_ExternalFunding:SetText(gsub(BarMath_CSG(BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["AdditionalMoney"]),",",""));
	BarMath_Money_Update();
end

function BarMath_Money_SetPrivateTarget_OnShow()
	local target;

	if (BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget") == 0) then
		target = 0;
	else
		target = BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget");
	end

	BarMath_MoneyOptions_SetPrivateTargetBox:SetText(gsub(BarMath_CSG(target),",",""))
	BarMath_Money_Update();
end

function BarMath_Money_SetPrivateTarget_OnChange()
	local money;
	local input = BarMath_MoneyOptions_SetPrivateTargetBox:GetText();
	local input2 = BarMath_TranslateToMoney(input);

	if (input2 ~= nil) then
		money = input2;
	else
		money = tonumber(input);
	end

	if (money ~= nil) then
		BarMath_SetCharVar(money,"Bars","MoneyBar","PrivateTarget");
		BarMath_MoneyOptions_AddPrivateToGlobalBtn:Enable();
	else
		BarMath_SetCharVar(0,"Bars","MoneyBar","PrivateTarget");
		BarMath_MoneyOptions_AddPrivateToGlobalBtn:Disable();
	end
	BarMath_MoneyOptions_SetPrivateTargetBox:SetText(gsub(BarMath_CSG(BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget")),",",""));
	BarMath_Money_Update();
end

function BarMath_MoneyOptions_CheckboxOnOff(checkbox, value)
	if (value == nil) then
		value = 0;
	end

	BarMath_Msg("Checkbox: "..tostring(checkbox)..", value: "..value,"debug");

	if checkbox == "BarMath_MoneyOptions_SelfishMode" then
		BarMath_SetCharVar(value,"Bars","MoneyBar","SelfishMode");
		if (value == 0) then
			BarMath_MoneyOptions_SelfishModeTooltipTitle = BARMATH_MONEY_TOOLTIPS_SELFISHMODEENABLETITLE;
		else
			BarMath_MoneyOptions_SelfishModeTooltipTitle = BARMATH_MONEY_TOOLTIPS_SELFISHMODEDISABLETITLE;
		end
		BarMath_Tooltip_Generate(BarMath_MoneyOptions_SelfishMode,getglobal("BarMath_MoneyOptions_SelfishModeTooltipTitle"),getglobal("BarMath_MoneyOptions_SelfishModeTooltipText"))
	end

	BarMath_Money_Update();
end

function BarMath_MoneyOptions_AddPrivateToGlobal()
	BarMath_Msg("Adding Private target to Global Target","debug");
	local privatemoney = BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget");
	local publicmoney = BarMath_GetAddonVar("Money",BarMath_Realm,BarMath_Faction,"Options","PublicTarget");
	
	publicmoney = publicmoney + privatemoney;
	BarMath_SetCharVar(0,"Bars","MoneyBar","PrivateTarget");
	BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"] = publicmoney;

	BarMath_MoneyOptions_SetPrivateTargetBox:SetText(gsub(BarMath_CSG(BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget")),",",""));
	BarMath_MoneyOptions_SetGlobalTargetBox:SetText(gsub(BarMath_CSG(BarMath_AddonData["Money"][BarMath_Realm][BarMath_Faction]["Options"]["PublicTarget"]),",",""));
	BarMath_MoneyOptions_AddPrivateToGlobalBtn:Disable();

	BarMath_Money_Update();
end