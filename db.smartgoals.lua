--[[
	BarMath 2.x Addon: Money Bar
	Smart Goal Database
	Version <%version%>
	
	Revision: $Id: db.smartgoals.lua 7 2012-12-23 11:49:55 PST Kjasi $

	All money costs are listed in copper. No costs include Rep Discounts.
]]

-- Multiplier for rep discount
BarMath_Money_RepDiscount = {
	["Neutral"] = 1,
	["Friendly"] = 0.95,
	["Honored"] = 0.9,
	["Revered"] = 0.85,
	["Exalted"] = 0.8,	
}

-- Riding Skill Costs.
BarMath_Money_RidingCosts = {
	["75"] = 350000,
	["150"] = 6000000,
	["225"] = 8000000,
	["300"] = 50000000,
	["375"] = 50000000,
}

-- Modifiers for Classes
-- The RepDiscount is a true/false for if a rep discount can be applied.
BarMath_Money_RidingCosts_Modified = {
	["Paladin"] = {
		["75"] = {
			["Amount"] = 10000,
			["RepDiscount"] = 1,
			["Reps"] = {
				BARMATH_MONEY_FACTION_STORMWIND,
				BARMATH_MONEY_FACTION_IRONFORGE,
				BARMATH_MONEY_FACTION_EXODAR,
				BARMATH_MONEY_FACTION_SILVERMOONCITY,
			},
		},
		["150"] = {
			["Amount"] = 3000000,
			["RepDiscount"] = 0,
			["Reps"] = {},
		},
	},
	["Warlock"] = {
		["75"] = {
			["Amount"] = 10000,
			["RepDiscount"] = 1,
			["Reps"] = {
				BARMATH_MONEY_FACTION_STORMWIND,
				BARMATH_MONEY_FACTION_UNDERCITY,
				BARMATH_MONEY_FACTION_ORGIMMAR,
				BARMATH_MONEY_FACTION_GNOMEREGANEXILES,
				BARMATH_MONEY_FACTION_IRONFORGE,
			},
		},
		["150"] = {
			["Amount"] = 4060000,
			["RepDiscount"] = 0,
			["Reps"] = {},
		},
	},
	["Druid"] = {
		["225"] = {
			["Amount"] = 81000,
			["RepDiscount"] = 1,
			["Reps"] = {
				BARMATH_MONEY_FACTION_CENARIONCIRCLE,
				BARMATH_MONEY_FACTION_THUNDERBLUFF,
				BARMATH_MONEY_FACTION_DARNASSUS,
			},
		},
	},
}

BarMath_Money_MountList = {
	--= Home Faction Mounts =--

	-- Standard Mounts

	-- Epic Mounts	

	-- Standard Flyers
	["25470"] = {				-- Golden Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},
	["25471"] = {				-- Ebon Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},
	["25472"] = {				-- Snowy Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},
	["25474"] = {				-- Tawny Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},
	["25475"] = {				-- Blue Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},
	["25475"] = {				-- Green Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},
	
	-- Epic Flyers
	["25473"] = {				-- Swift Blue Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25527"] = {				-- Swift Red Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25528"] = {				-- Swift Green Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25529"] = {				-- Swift Purple Gryphon
		["Faction"] = "Alliance",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25477"] = {				-- Swift Red Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25531"] = {				-- Swift Green Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25532"] = {				-- Swift Yellow Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},
	["25533"] = {				-- Swift Purple Windrider
		["Faction"] = "Horde",
		["Rep"] = "None",
		["RepCost"] = "Neutral",
		["Cost"] = 2000000,
		["Discount"] = 0,
	},


	--= Special Faction Mounts =--

	-- Standard Mounts

	-- Epic Mounts
	["13086"] = {				-- Winterspring Frostsaber
		["Faction"] = "Alliance",
		["Rep"] = BARMATH_MONEY_FACTION_WINTERSABER_TRAINERS,
		["RepCost"] = "Exalted",
		["Cost"] = 1000000,
		["Discount"] = 0,
	},

	-- Standard Flyers

	-- Epic Flying
	["33999"] = {				-- Cenarion War Hippogryph
		["Faction"] = "Both",
		["Rep"] = BARMATH_MONEY_FACTION_CENARIONEXPEDITION,
		["RepCost"] = "Exalted",
		["Cost"] = 20000000,
		["Discount"] = 1,
	},
}