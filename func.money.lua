--[[
	BarMath 2.x Addon: Money Bar
	Money Bar Functions
	Version <%version%>
	
	Revision: $Id: func.money.lua 7 2012-12-23 11:51:14 PST Kjasi $
]]

local BMGold_Version = "<%version%>";
BarMath_Money = 0;

-- Defaults, specialized for the Money Bar
local BarMath_Defaults_Money = {
	["Display"] = 1,			-- 1 = On, 0 = Off
	["DisplayMethod"] = 1,			-- 1 = Text is Always on, 0 = Text only when mouseover
	["TextLength"] = BARMATH_TEXT_SHORT,	-- Other Value: BARMATH_TEXT_LONG
	["Displays"] = {
		["Display1"] = 1,		-- Determines the default option for the Displays.
		["Display2"] = 6,
		["Display3"] = 3,
		["Display4"] = 4,
		["Display5"] = 5,
		["Display1OnOff"] = 1,		-- Default setting for if this Display is visible. 1 = Visible, 0 = Hidden.
		["Display2OnOff"] = 1,
		["Display3OnOff"] = 1,
		["Display4OnOff"] = 1,
		["Display5OnOff"] = 1,					
	},
	["PrivateTarget"] = 0,			-- Private Target; For when you want to use money seperate from the global target
	["SelfishMode"] = 0,			-- Selfish Mode; Hides your gold from other characters
	["Cutoff"] = "c",			-- Show only money under this. Other Values: "g" & "s".
	["ListBothFactions"] = 0,		-- Show Both Factions in the Money List
	["ListAllServers"] = 0,			-- Show All Servers in the Money List
}
-- Info is for globals that all characters share.
local BarMath_Defaults_Money_Info = {
	[BarMath_Realm] = {
		[BarMath_Faction] = {
			["Options"] = {				
				["PublicTarget"] = 0,		-- A Global money target. This is shared by all characters, unless they have a private target.
				["AdditionalMoney"] = 0, 	-- Another source of money not found on this account.
				["IncludeBothFactions"] = 0,
				["IncludeAllServers"] = 0,
			},
			["Players"] = {},
		},
	},
}

-- Generate Global Info
-- Currently Supports up to 7 levels of Data
BarMath_BuildAddonDB("Money",BarMath_Defaults_Money_Info);

-- Add Bar to Barlist
	-- First Variable is the Bar's Name. With our Money example, the final name for the bar will be "BarMathMoneyBar".
	-- Second is the update function. This is called to update the bar's data.
	-- Third is the default option settings for this bar.
	-- Fourth variable is the parent. If not set, or the bar doesn't exist, then BarMath will automatically assign a parent.
	-- Fifth variable is the InfoDelete function that should be called when deleting a character's information from the Info Database. Realm, Faction and Character Name is passed to this function. (In that order.) Should return a true variable if successful.
	-- Sixth is the template this bar should follow. This is useful for if you want to make your own bar template, say with 4 bars instead of 1.
BarMath_AddBar("Money", "BarMath_Money_Update", BarMath_Defaults_Money,"Rep","BarMath_Money_InfoDelete");


--= Option Text =--
-- The following is Option Text. All of them MUST be in an identical order to !
-- BarMath's Localization has some common Display Options you can use to reduce your own localizations.

-- Dropdown Text
BarMath_Option_DropDown_Money_Listing = {
	"BARMATH_DISPLAYOPTION_BAR_FILLED", 		-- Amount of Bars Filled
	"BARMATH_DISPLAYOPTION_MONEY_BAR_PERBAR",	-- Gold Per Bar
	"BARMATH_DISPLAYOPTION_CURRMAX",		-- The Current / The Maximum
	"BARMATH_DISPLAYOPTION_MONEY_BAR_TOLVL",	-- Amount Needed until Goal is Reached
	"BARMATH_DISPLAYOPTION_MONEY_BAR_TOFILL",	-- Bars needed to Level
	"BARMATH_DISPLAYOPTION_PERC_FILLED",		-- Percent Filled
	"BARMATH_DISPLAYOPTION_MONEY_PERC_TOFILL",	-- Percent Needed until Goal is Reached
	"BARMATH_DISPLAYOPTION_MONEY_PERC_PERBAR",	-- Gold Per Percent
};

-- Money Update function.
function BarMath_Money_Update()
	BarMath_UpdateMoney();
	if (BarMath_HaveLoaded["Bars"]["Money"]) then
		local target = 0;

		local selfishmode = BarMath_GetCharVar("Bars","MoneyBar","SelfishMode");
		local privtarget = BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget");

		if type(privtarget) ~= "number" or type(selfishmode) ~= "number" then
			return;
		end

		if (privtarget > 0) or (selfishmode == 1) then
			target = BarMath_GetCharVar("Bars","MoneyBar","PrivateTarget");
		else
			target = BarMath_GetAddonVar("Money",BarMath_Realm,BarMath_Faction,"Options","PublicTarget");
		end

		-- BarMath_Msg("Current Money Target: "..BarMath_CSG(target),"debug")

		if (target <= 0) then
			BarMathMoneyBar:Hide();
		--	BarMath_Update_All();
			return;
		end
		BarMathMoneyBar:Show();

		local low,high = BarMath_Money, target;
		local min2,max = BarMath_Money, target;

		BarMath_Msg("Target Amount Recorded: \""..tostring(high).."\"","debug");

		local togostring, togostring2, perbarstring, adjustment;

		if target < 100 then
			togostring = BARMATH_TXT_COPPER;
		elseif target < 10000 then
			togostring = BARMATH_TXT_SILVER;
		else
			togostring = BARMATH_TXT_GOLD;
		end

		if (BarMath_GetCharVar("Bars","MoneyBar","TextLength") == BARMATH_TEXT_LONG) then
			togostring = togostring..BARMATH_BARTXT_TOGO;
			perbarstring = BARMATH_BARTXT_PERBAR_MONEY;
		else
			togostring = BARMATH_BARTXT_TOGO;
			perbarstring = BARMATH_BARTXT_PERBAR;
		end

		local thismin = BarMath_TranslateToMoney(BarMath_CSG(min2));
		
		local thismax = BarMath_TranslateToMoney(BarMath_CSG(max));
		local thismax2, adjustment = BarMath_CheckTarget(thismax);

		BarMath_Msg(tostring(thismax)..' VS '..tostring(thismax2),"debug");

		if min2 >= max then
			if BarMath_GetCharVar("Bars","MoneyBar","TextLength") == "Long" then
				togostring2 = BARMATH_BARTXT_GOLD_REACHED_GOAL;
			else
				togostring2 = BARMATH_BARTXT_GOLD_GOAL_ACHIEVED;
			end
		else
			if thismin == nil then
				thismin = 0;
			end
			if thismax == nil then
				thismax = 0;
			end
			togostring2 = togostring..": "..BarMath_CSG((thismax-thismin));
		end
		if adjustment == 1 then
			adjustment = " (*)";
		else
			adjustment = "";
		end

		if thismax == nil then
			max2 = BarMath_CheckTarget(max);
		else
			max2 = max;
		end

		-- Dah Math!
		local perbar, bars, togo, XPtogo, pcent, pcent_togo = BarMath_Money_Calculate(target);

		local option = {}
		local display = {};
		local post;
		local goldperpcent = BarMath_CSG(BarMath_Round((max/100)));

		if (BarMath_GetCharVar("Bars","MoneyBar","TextLength") == BARMATH_TEXT_SHORT) then
			txt = BARMATH_BARTXT_MONEY_SHORT;
		else
			txt = BARMATH_BARTXT_MONEY_LONG;
		end

		option[1] = txt[1]..BarMath_AddCommas(bars);
		option[2] = txt[2]..BarMath_CSG(perbar);
		option[3] = BarMath_CSG(low).." / "..BarMath_CSG(high)..adjustment;
		option[4] = txt[3]..BarMath_CSG(XPtogo);
		option[5] = txt[4]..BarMath_AddCommas(togo);
		option[6] = txt[5]..BarMath_AddCommas(pcent).."%";
		option[7] = txt[6]..BarMath_AddCommas(pcent_togo).."%";
		option[8] = txt[7]..goldperpcent;

		if (min2 >= max2) then
			option[4] = txt[8];
			option[5] = txt[8];
		end
		-- Determine which options need to be displayed.
		for i=1, BarMath.DisplayCount, 1 do
			local thisopt = BarMath_GetCharVar("Bars","MoneyBar","Displays","Display"..i);
			display[i] = tostring(option[thisopt]);
		end

		-- Set the Displays!
		for i=1, BarMath.DisplayCount, 1 do
			BarMath_SetBarText(display[i], "Money", i)
		end

		-- Set the Displays!
		BarMathMoneyBar:SetStatusBarColor(1.0, 0.7, 0.3, 1.0);
		BarMathMoneyBar:SetMinMaxValues(0, high);
		BarMathMoneyBar:SetValue(low);

		-- Run Show/Hide to finalize any changes
		BarMath_Bar_ShowHide("Money");
	end

--[[

	if (BarMath_HaveLoaded["Bars"]["Money"]) then
		-- Call function to see how many times this is ran. Should be deleted or commented out before release!
		BarMath_Msg("Money Bar Loaded...","debug");

		-- Set Locals
		local post;
		local option, display = {}, {};
		local currXP, nextXP = UnitXP("player"), UnitXPMax("player");
		local maxlvl = 70;

		-- Figure out everything!
		local perbar, bars, togo, XPtogo, pcent, pcent_togo = BarMath_XP_Calculate();
		local xpperpcent = BarMath_Round(nextXP/100);
		local restedxp = GetXPExhaustion();
		if restedxp == nil then
			restedxp = BARMATH_TXT_NONE;
		end
		local xptomax = BarMath_XPtoMax();
		local barstomax = ((maxlvl-UnitLevel("player"))*BarMath.NumBars)-bars;

		-- Determine what text to use if the text length is short or long.
		if (BarMath_GetCharVar("Bars", "XPBar", "TextLength") == BARMATH_TEXT_SHORT) then
			txt = BARMATH_TXT_XP_SHORT;
		else
			txt = BARMATH_TXT_XP_LONG;
		end

		-- Set all the options!
		option[1] = txt[1]..bars;
		option[2] = txt[2]..perbar;
		option[3] = txt[3]..currXP.." / "..nextXP;
		option[4] = txt[4]..XPtogo;
		option[5] = txt[5]..togo;
		option[6] = txt[6]..pcent.."%";
		option[7] = txt[7]..pcent_togo.."%";
		option[8] = txt[8]..xpperpcent;
		option[9] = txt[9]..restedxp;
		option[10] = txt[10]..xptomax;
		option[11] = txt[11]..barstomax;

		-- Determine which options need to be displayed.
		for i=1, BarMath.DisplayCount, 1 do
			local thisopt = BarMath_GetCharVar("Bars","XPBar","Displays","Display"..i);
			display[i] = tostring(option[thisopt]);
		end

		-- Set the Displays!
		for i=1, BarMath.DisplayCount, 1 do
			BarMath_SetBarText(display[i], "XP", i)
		end

		-- Set the Bar!
		BarMathXPBar:SetMinMaxValues(min(0, currXP), nextXP)
		BarMathXPBar:SetValue(currXP);

		local exhaustionStateID = GetRestState();
		if ((exhaustionStateID == 2) or (restedxp == 0)) then
			BarMathXPBar:SetStatusBarColor(0.58, 0.0, 0.55, 1.0);
		elseif (exhaustionStateID == 1) then
			BarMathXPBar:SetStatusBarColor(0.0, 0.39, 0.88);
		end

		-- Run Show/Hide to finalize any changes
		BarMath_Bar_ShowHide("XP");
	end
]]
end

--== Money Bar Functions ==--

-- Translate from xxxxxx Copper to xx Gold, xx Silver, xx Copper.
function BarMath_TranslateMoney(money)
	if money == nil then
		money = "0c";
	else
		money = tostring(money);
	end
	local c, s, g = nil;
	
	c = strsub(money,-2);
	if (strlen(money)>2) then
		s = strsub(money,-4,-3)
	end
	if (strlen(money)>4) then
		g = strsub(money,0,strlen(money)-4)
	end
	return tonumber(c), tonumber(s), tonumber(g);
end

-- Translate from xx Gold, xx Silver, xx Copper to xxxxxx Copper.
function BarMath_TranslateToMoney(string)
	string = tostring(string);
	local cmon, smon, gmon = 0;
	local gstart = strfind(string,"g");
	local sstart = strfind(string,"s");
	local cstart = strfind(string,"c");

	if gstart ~= nil then
		sstr = strsub(string,0,gstart-1);
		gmon = tonumber(sstr);
	else
		gmon = 0;
		gstart = -1;
	end

	if sstart ~= nil then
		sstr = strsub(string,gstart+1,sstart-1);
		smon = tonumber(sstr);
	else
		smon = 0;
		if gstart ~= nil then
			sstart = gstart;
		else
			sstart = -1;
		end
	end
	if cstart ~= nil then
		sstr = strsub(string,sstart+1,cstart-1);
		cmon = tonumber(sstr);
	else
		cmon = 0;
		cstart = -1;
	end

	if (gmon == nil) then
		gmon = 0;
	end
	if (smon == nil) then
		smon = 0;
	end
	if (cmon == nil) then
		cmon = 0;
	end

	BarMath_Msg("Mons: G='"..gmon.."', S='"..smon.."', C='"..cmon.."'" , "debug");

	if (gmon ~= nil and smon ~= nil and cmon ~= nil) then
		gmon = tonumber(gmon);
		smon = tonumber(smon);
		cmon = tonumber(cmon);
		if (gmon > 0 or smon > 0 or cmon > 0) then
			local final = cmon + (smon * COPPER_PER_SILVER) + (gmon * COPPER_PER_GOLD);
			return final;
		else
			return nil;
		end
	else
		return nil;
	end
end

-- Take BarMath_TranslateMoney's c, s, g and output it as xxGxxSxxC
function BarMath_CSG(money)
	local c, s, g = BarMath_TranslateMoney(money);
	local text = "";
	if (g ~= nil) then
		text = BarMath_AddCommas(g)..BARMATH_TXT_GOLD_S;
	end
	if (s ~= nil) and (s ~= "00") and (BarMath_GetCharVar("Bars","MoneyBar","Cutoff") ~= "g") then
		text = text..s..BARMATH_TXT_SILVER_S;
	end
	if (c ~= nil) and (c ~= "00") and (BarMath_GetCharVar("Bars","MoneyBar","Cutoff") ~= "g") and (BarMath_GetCharVar("Bars","MoneyBar","Cutoff") ~= "s")  then
		text = text..c..BARMATH_TXT_COPPER_S;
	end
	return text;
end

-- Return the Total amount of money available, based on current options.
function BarMath_UpdateMoney()
	local total_cash = 0;
	BarMath_SetAddonVar("Money",GetMoney(),BarMath_Realm,BarMath_Faction,"Players",BarMath_PlayerName);

	for realm, v in pairs(BarMath_AddonData["Money"]) do
		if ((BarMath_GetCharVar("Bars","MoneyBar","ListAllServers")==1)or((BarMath_GetCharVar("Bars","MoneyBar","ListAllServers")==0) and (realm == BarMath_Realm))) then
			for faction, v in pairs(BarMath_AddonData["Money"][realm]) do
				if ((BarMath_GetCharVar("Bars","MoneyBar","ListBothFactions")==1)or((BarMath_GetCharVar("Bars","MoneyBar","ListBothFactions")==0)and(faction == BarMath_Faction))) then
					for player, cash in pairs(BarMath_AddonData["Money"][realm][faction]["Players"]) do
						if ((BarMath_GetCharVar("Bars","MoneyBar","SelfishMode") == 0)or(player == BarMath_PlayerName)) then
							total_cash = total_cash + cash;
						end
					end
				end
			end
		end
	end

	BarMath_Money = total_cash + BarMath_GetAddonVar("Money",BarMath_Realm,BarMath_Faction,"Options","AdditionalMoney");
	-- BarMath_Msg("Current Funds: "..BarMath_CSG(BarMath_Money),"debug");
end

-- Calculation Function
function BarMath_Money_Calculate(max)
	local perbar, bars, togo, phave, pneed, current, max2;

	current = BarMath_Money;

	perbar = BarMath_Round(max/BarMath.NumBars);
	XPTogo = max - current;

	local amount = (current/max)*100;
	phave = BarMath_Round(amount);
	pneed = BarMath_Round(100-phave);
	bars = BarMath_Round(current/perbar);
	togo = BarMath_Round(BarMath.NumBars-bars);

	return perbar, bars, togo, XPTogo, phave, pneed;
end

-- Checks to see if the amount passed is below the cut off point.
function BarMath_CheckTarget(check)
	local output, adjustment = 0;
	local money = tonumber(check);
	if money == nil then
		money = 0;
	end

	BarMath_Msg("Check: "..tostring(check)..", Money: "..money, "debug")

	local a_gold = 1 * COPPER_PER_GOLD;
	local a_silver = 1 * COPPER_PER_SILVER;

	if (money < a_gold) and (BarMath_GetCharVar("Bars","MoneyBar","Cutoff") == "g") then
		output = a_gold;
		adjustment = 1;
	elseif (money < a_silver) and (BarMath_GetCharVar("Bars","MoneyBar","Cutoff") == "s") then
		output = a_silver;
		adjustment = 1;
	end

	return output, adjustment;
end

-- Delete data in the Information database.
function BarMath_Money_InfoDelete(Realm, Faction, Char)
	local fcount, pcount, isp2bd, isf2bd, newinfo = 0, 0, false, false, {};

	for f,v in pairs(BarMath_AddonData["Money"][Realm]) do
		fcount = fcount + 1;
		if f == Faction then
			isf2bd = true;
		end
	end

	for player,v in pairs(BarMath_AddonData["Money"][Realm][Faction]["Players"]) do
		pcount = pcount + 1;
		if player == Char then
			isp2bd = true;
		end
	end

	if ((fcount <= 1) and (pcount <= 1) and (isf2bd == true)) then
		for s,v in pairs(BarMath_AddonData["Money"]) do
			if (s ~= Realm) then
				newinfo[s] = BarMath_AddonData["Money"][s];
			end
		end
		BarMath_AddonData["Money"] = newinfo;
		return "true";
	end

	if (pcount <= 1 and isp2bd == true) then
		for s,v in pairs(BarMath_AddonData["Money"]) do
			if (not newinfo[s]) then newinfo[s] = {}; end
			for f,v in pairs(BarMath_AddonData["Money"][s]) do
				if (f ~= Faction) then
					newinfo[s][f] = BarMath_AddonData["Money"][s][f];
				end
			end
		end
		BarMath_AddonData["Money"] = newinfo;
		return "true";
	end

	if (pcount > 1 and isp2bd == true) then
		for s,v in pairs(BarMath_AddonData["Money"]) do
			if (not newinfo[s]) then newinfo[s] = {}; end
			for f,v in pairs(BarMath_AddonData["Money"][s]) do
				if (not newinfo[s][f]) then newinfo[s][f] = {} end;
				newinfo[s][f]["Options"] = BarMath_AddonData["Money"][s][f]["Options"];
				newinfo[s][f]["Players"] = {};
				for p,v in pairs(BarMath_AddonData["Money"][s][f]["Players"]) do
					if (p ~= Char) then
						newinfo[s][f]["Players"][p] = BarMath_AddonData["Money"][s][f]["Players"][p];
					end
				end
			end
		end
		BarMath_AddonData["Money"] = newinfo;
		return "true";
	end

	return;
end


--[[ Option Window Data ]]--

-- Option Window information;
function BarMath_Money_Options()
	-- Used for listing tabs & their templates.
	local tabs = {
		[1] = {
			["TabTitle"] = "Settings",
			["TabTemp"] = "BarMath_Money_Tab_Goals",
		},
--[[
Not Yet Implemented, but in here so you can see how to add multiple tabs.
		[2] = {
			["TabTitle"] = "Smart Goals",
			["TabTemp"] = "BarMath_Money_Tab_SmartGoals",
		},
		[3] = {
			["TabTitle"] = "Gold List",
			["TabTemp"] = "BarMath_Money_Tab_GoldList",
		}
]]
	};
	local BarTitle = "Money"; 			-- Used internally. Doesn't need localization.
	local displayname = BARMATH_MONEY_TITLE; 	-- The name of the Addon. Used in the Options list.
	BarMath_Generate_Options(BarTitle,displayname,BarMath_Option_DropDown_Money_Listing,tabs);
end

-- Load Option Window information!
BarMath_Money_Options();


--[[ Function Hooks ]]--
BarMath_AddEvent("PLAYER_LOGIN",BarMath_Money_Update);
BarMath_AddEvent("MAIL_SHOW",BarMath_Money_Update);
BarMath_AddEvent("MAIL_CLOSED",BarMath_Money_Update);
BarMath_AddEvent("PLAYER_MONEY",BarMath_Money_Update);
BarMath_AddEvent("CHAT_MSG_MONEY",BarMath_Money_Update);
BarMath_AddEvent("PLAYER_TRADE_MONEY",BarMath_Money_Update);

-- Loaded Message
-- Comes last to ensure that everything has been loaded first!
BarMath_HaveLoaded["Addons"]["Money"] = "Money Addon v"..BMGold_Version;