--[[
	BarMath 2.x Addon: Money Bar
	Smart Goal Functions
	Version <%version%>
	
	Revision: $Id: func.smartgoals.lua 7 2012-12-23 11:52:14 PST Kjasi $
]]

function BarMathMoney_GetRidingSkill()
	local numSkills = GetNumSkillLines();
	for i=1, numSkills do
		local skillName, _, _, skillRank = GetSkillLineInfo(i);
		if skillnName == "Riding" then
			return skillRank;
		end
	end
	return;
end

function BarMathMoney_GetBestRidingRepDiscount()
	local cRep,cStandID = nil, 0;
	local numfact = GetNumFactions();

	for x=1,numfact do
		local name, _, standingId, _, _, _, _, _, isHeader = GetFactionInfo();
		if (not isHeader) then
			if (BarMath_Class == "Druid") and (BarMathMoney_GetRidingSkill() == 150) and (name == BARMATH_MONEY_FACTION_CENARIONCIRCLE) then
				if (standingId > cStandID) then
					cRep = name;
					cStandID = standingId;
				end
			elseif (BarMathMoney_GetRidingSkill() < 150) then
				if (name == BARMATH_MONEY_FACTION_STORMWIND) or (name == BARMATH_MONEY_FACTION_IRONFORGE) or (name == BARMATH_MONEY_FACTION_EXODAR) or (name == BARMATH_MONEY_FACTION_DARNASSUS) or (name == BARMATH_MONEY_FACTION_GNOMEREGANEXILES) or (name == BARMATH_MONEY_FACTION_SILVERMOONCITY) or (name == BARMATH_MONEY_FACTION_UNDERCITY) or (name == BARMATH_MONEY_FACTION_THUNDERBLUFF) or (name == BARMATH_MONEY_FACTION_ORGIMMAR) or (name == BARMATH_MONEY_FACTION_DARKSPEARTROLLS) then
					if (standingId > cStandID) then
						cRep = name;
						cStandID = standingId;
					end
				end
			end
		end
	end

	if (cRep == nil) then
		cRep = "None";
	end
	if (cStandID < 4) then
		cStandID = 4;
	end

	local discount = BarMath_Money_RepDiscount[getglobal("FACTION_STANDING_LABEL"..cStandID)];

	return cRep,discount;
end

function BarMathMoney_GetRidingCost()
	local RidingSkill = BarMathMoney_GetRidingSkill();
	local RidingCosts = BarMath_Money_RidingCosts;
	local RepName;

	-- Apply Class Modifier & Modifier
	if BarMath_Money_RidingCosts_Modified[BarMath_Class] then
		if BarMath_Money_RidingCosts_Modified[BarMath_Class]["75"] then
			if BarMath_Money_RidingCosts_Modified[BarMath_Class]["75"]["RepDiscount"] == 1 then
				local discount = 1;
				RepName, discount = BarMathMoney_GetBestRidingRepDiscount();
				local finalcost = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["75"]["Amount"] * discount;
				RidingCosts["75"] = finalcost;
			else
				RidingCosts["75"] = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["75"]["Amount"];
			end
		end
		if BarMath_Money_RidingCosts_Modified[BarMath_Class]["150"] then
			if BarMath_Money_RidingCosts_Modified[BarMath_Class]["150"]["RepDiscount"] == 1 then
				local discount = 1;
				RepName, discount = BarMathMoney_GetBestRidingRepDiscount();
				local finalcost = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["150"]["Amount"] * discount;
				RidingCosts["150"] = finalcost;
			else
				RidingCosts["150"] = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["150"]["Amount"];
			end
		end
		if BarMath_Money_RidingCosts_Modified[BarMath_Class]["225"] then
			if BarMath_Money_RidingCosts_Modified[BarMath_Class]["225"]["RepDiscount"] == 1 then
				local discount = 1;
				RepName, discount = BarMathMoney_GetBestRidingRepDiscount();
				local finalcost = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["225"]["Amount"] * discount;
				RidingCosts["225"] = finalcost;
			else
				RidingCosts["225"] = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["225"]["Amount"];
			end
		end
		if BarMath_Money_RidingCosts_Modified[BarMath_Class]["300"] then
			if BarMath_Money_RidingCosts_Modified[BarMath_Class]["300"]["RepDiscount"] == 1 then
				local discount = 1;
				RepName, discount = BarMathMoney_GetBestRidingRepDiscount();
				local finalcost = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["300"]["Amount"] * discount;
				RidingCosts["300"] = finalcost;
			else
				RidingCosts["300"] = BarMath_Money_RidingCosts_Modified[UnitClass("player")]["300"]["Amount"];
			end
		end
	end

	return RidingCosts;
end