## Title: BarMath [|CFF5e88ffMoney|r]
## Interface: 50100
## Version: <%version%>
## Author: Kjasi
## Notes: Money Addon for BarMath
## URL: http://code.google.com/p/kjasiwowaddons/
## DefaultState: Disabled
## Dependancies: BarMath
## X-Category: Information
localization.lua
func.money.lua
db.smartgoals.lua
options.lua
options.xml
