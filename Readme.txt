Money Bar Addon v<%version%>, for BarMath 2.x


-= Troubleshooting =-

Q: There's a (*) on the money bar. What does it mean?
A: This is a signal that your target money is less than your cutoff. This will appear if you have a target of 4 silver, but you cut off all but gold.

Q: Something's wrong with the money bar. It alway says I have xx more gold than I do!
A: First off, the Money bar will read the money from ALL your toons on that server, not just your current one. The most likely thing is that you have xx gold (combined) on your other toons. Try turning on Selfish Mode. If the gold dissappears, then it's probably on your other toons. However, if you definatly don't have that much on your other toons combined, then it's possible that the money bar thinks that one of your toons has more money than it does. Try logging back into all your toons, and checking your gold. This should fix the issue.

-= To Do List =-
 - Bring the Cutoff option into the Options Panel. Cutoff will reduce the money displayed to the nearest copper/silver/gold, as selected by you.
 - Add a Character Gold List. This will list all your characters, across all your realms, Horde and Alliance alike, and their current money amount, weither they're in selfish mode or not.
 - Add a Riding Skill Smart Goal. This will automatically add the riding skill price to your current goals, including reputation deductions, and is Class-Smart. (The price will be different for Druids, Warlocks and Paladins, who get Riding levels for free.)
 - Add a List of common items to add to the Money Goal, such as various mounts, the Cenarion War Hippogryph, the Motorcycle and other items.

-= Version History =-
v<%version%>
 - Comma Support

v1.0
 - Updated to BarMath 2.0
 - Switched to a Global Money Target. This will give you the same target across all your characters on that realm.
 - Added Private Money Target. This will over-ride the Global if set, or if in Selfish Mode.
 - Added an option to include money not found on your current account. This amount must be manually controlled.