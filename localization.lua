--[[
	BarMath 2.x Addon: Money Bar
	English Localization
	Version <%version%>
	
	Revision: $Id: localization.lua 7 2012-12-23 11:53:06 PST Kjasi $
]]

BARMATH_MONEY_TITLE = "Money";

-- Display Options
BARMATH_DISPLAYOPTION_MONEY_BAR_FILLED = "Amount of Bars Filled";
BARMATH_DISPLAYOPTION_MONEY_BAR_PERBAR = "Gold Per Bar";
BARMATH_DISPLAYOPTION_MONEY_BAR_TOLVL = "Gold Needed to Reach Goal"; --  (Max - Current)
BARMATH_DISPLAYOPTION_MONEY_BAR_TOFILL = "Bars to Goal";
BARMATH_DISPLAYOPTION_MONEY_PERC_TOFILL = "Percentage Needed for Goal";
BARMATH_DISPLAYOPTION_MONEY_PERC_PERBAR = "Gold Per Percent";


-- Text that appears on the Bars
-- Long Text
BARMATH_BARTXT_MONEYL_BARS_FILLED = "Bars Filled: ";
BARMATH_BARTXT_MONEYL_PERBAR = "Gold Per Bar: ";
BARMATH_BARTXT_MONEYL_TO_GOAL = "Amount Until Goal is Reached: ";
BARMATH_BARTXT_MONEYL_BARS_TO_GOAL = "Bars Until Goal is Reached: ";
BARMATH_BARTXT_MONEYL_PERCENT_EARNED = "Percentage Obtained: ";
BARMATH_BARTXT_MONEYL_PERCENT_NEEDED = "Percentage Needed for Goal: ";
BARMATH_BARTXT_MONEYL_PER_PERCENT = "Gold Per Percent: ";
BARMATH_BARTXT_MONEYL_REACHED_GOAL = "You have reached your goal!";

-- Short Text
BARMATH_BARTXT_MONEYS_TO_GOAL = "To Go: ";
BARMATH_BARTXT_MONEYS_BARS_TO_GOAL = "Bars To Go: ";
BARMATH_BARTXT_MONEYS_PER_PERCENT = "Gold Per %: ";
BARMATH_BARTXT_MONEYS_REACHED_GOAL = "Goal Achieved!";

-- GUI Text
BARMATH_MONEY_GLOBALTARGET = "Global Target Amount";
BARMATH_MONEY_PRIVATETARGET = "Private Target Amount";
BARMATH_MONEY_SELFISHMODE = "Selfish Mode";
BARMATH_MONEY_ADDPRIVATETOGLOBAL = "Add to Global Target";
BARMATH_MONEY_EXTERNALFUNDING = "Additional Funding";

-- GUI Tooltips
BARMATH_MONEY_TOOLTIPS_SETGLOBALTITLE = "Set Global Goal";
BARMATH_MONEY_TOOLTIPS_SETGLOBALTEXT = "Specify a goal to be shared by\nall your toons on this server.";
BARMATH_MONEY_TOOLTIPS_SETPRIVATETITLE = "Set Private Goal";
BARMATH_MONEY_TOOLTIPS_SETPRIVATETEXT = "Specify a goal only to be used by this toon.\n\nAny value other than 0c will cause this goal\nto be used instead of the global target.";
BARMATH_MONEY_TOOLTIPS_SELFISHMODEENABLETITLE = "Enable Selfish Mode";
BARMATH_MONEY_TOOLTIPS_SELFISHMODEDISABLETITLE = "Disable Selfish Mode";
BARMATH_MONEY_TOOLTIPS_SELFISHMODETEXT = "When active, your toon will only display it's\nown money, ignoring what your other toons\nmay have. Likewise, your other toons will\nignore how much money this one has.\n\nSelfish Mode requires the use of a Private\nTarget in order to have a goal. The Global\nTarget will be ignored.";
BARMATH_MONEY_TOOLTIPS_EXTERNALFUNDINGTEXT = "This is used as an additional source\nof funding, usually a different account,\nor money from a guild bank.";

-- Rep Factions, for Rep Discounts
BARMATH_MONEY_FACTION_STORMWIND = "Stormwind";
BARMATH_MONEY_FACTION_IRONFORGE = "Ironforge";
BARMATH_MONEY_FACTION_EXODAR = "Exodar";
BARMATH_MONEY_FACTION_DARNASSUS = "Darnassus";
BARMATH_MONEY_FACTION_GNOMEREGANEXILES = "Gnomeregan Exiles";
BARMATH_MONEY_FACTION_SILVERMOONCITY = "Silvermoon City";
BARMATH_MONEY_FACTION_UNDERCITY = "Undercity";
BARMATH_MONEY_FACTION_THUNDERBLUFF = "Thunder Bluff";
BARMATH_MONEY_FACTION_ORGIMMAR = "Orgimmar";
BARMATH_MONEY_FACTION_DARKSPEARTROLLS = "Darkspear Trolls";
BARMATH_MONEY_FACTION_CENARIONCIRCLE = "Cenarion Circle";

-- Rep Factions for Mounts
BARMATH_MONEY_FACTION_CENARIONEXPEDITION = "Cenarion Expedition"; -- For Hippogryph mount.
BARMATH_MONEY_FACTION_WINTERSABER_TRAINERS = "Wintersaber Trainers";

BARMATH_TXT_COPPER_S = "c";
BARMATH_TXT_SILVER_S = "s";
BARMATH_TXT_GOLD_S = "g";
BARMATH_TXT_COPPER = "Copper";
BARMATH_TXT_SILVER = "Silver";
BARMATH_TXT_GOLD = "Gold";

BARMATH_MONEY_INCLUDE_OPFAC_HORDE = "Include money from your Horde Characters";
BARMATH_MONEY_INCLUDE_OPFAC_ALLIANCE = "Include money from your Alliance Characters";

BARMATH_MONEY_INCLUDE_OPFAC_TOOLTIP = "Include money from the other faction in the money calculations.\n\nNote: This will only work if Selfish mode is turned off.";

BARMATH_MONEY_SELFISH = "Selfish Mode";
BARMATH_MONEY_SELFISH_TOOLTIP = "When activated, Selfish mode will\nonly read money information on your\ncurrent character, and ignore the\nmoney on your other characters.\n\nMoney from this toon will still show\nup on other toons, even if you've\nenabled selfish mode on this toon.";

BARMATH_BARTXT_PERCENT_TO_GOAL = "Percent Needed to Reach Goal: ";
BARMATH_BARTXT_PERBAR_MONEY = "Money Per Bar: ";
BARMATH_BARTXT_GOLD_TO_GOAL = "Amount Until Goal is Reached: ";
BARMATH_BARTXT_GOLD_TO_GO = "To Go: ";
BARMATH_BARTXT_GOLD_BARS_TO_GOAL = "Bars Until Goal is Reached: ";
BARMATH_BARTXT_GOLD_BARS_TO_GO = "Bars To Go: ";
BARMATH_BARTXT_GOLD_PER_PERCENT = "Money Per Percent: ";
BARMATH_BARTXT_GOLD_PER_P = "Money Per %: ";
BARMATH_BARTXT_GOLD_GOAL_ACHIEVED = "Goal Achieved!";
BARMATH_BARTXT_GOLD_REACHED_GOAL = "You have reached your goal!";

BARMATH_BARTXT_MONEY_LONG = {
	BARMATH_BARTXT_BARSFILLED,
	BARMATH_BARTXT_PERBAR_MONEY,
	BARMATH_BARTXT_GOLD_TO_GOAL,
	BARMATH_BARTXT_GOLD_BARS_TO_GOAL,
	BARMATH_BARTXT_PERCENT_EARNED,
	BARMATH_BARTXT_PERCENT_TO_GOAL,
	BARMATH_BARTXT_GOLD_PER_PERCENT,
	BARMATH_BARTXT_GOLD_REACHED_GOAL,
};

BARMATH_BARTXT_MONEY_SHORT = {
	BARMATH_BARTXT_BARS,
	BARMATH_BARTXT_PERBAR,
	BARMATH_BARTXT_GOLD_TO_GO,
	BARMATH_BARTXT_GOLD_BARS_TO_GO,
	BARMATH_BARTXT_PERCENT,
	BARMATH_BARTXT_P_NEEDED,
	BARMATH_BARTXT_GOLD_PER_P,
	BARMATH_BARTXT_GOLD_GOAL_ACHIEVED,
};